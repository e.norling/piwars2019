import time
from threading import Event, Thread
from bluetooth import *

class Comms():
    def __init__(self, ready=None):
        self.ready = ready

    def connect(self):
        server_addr = "B8:27:EB:1E:6A:88" #glitterdriver
        #server_addr = "B8:27:EB:93:57:A6" #glittertest
        uuid = "5d122e1e-00fc-40af-82cc-bc71c6844a0f"
        found = False
        while not found:
            service_matches = find_service( uuid = uuid, address = server_addr )

            found = len(service_matches) != 0
            if not found:    
                print ("couldn't find the DriverServer service =(")
                time.sleep(1)

        first_match = service_matches[0]
        port = first_match["port"]
        name = first_match["name"]
        host = first_match["host"]

        print ("connecting to \"%s\" on %s" % (name, host))

        # Create the client socket
        connected = False
        while not connected:
            try:
                self.socket=BluetoothSocket( RFCOMM )
                self.socket.connect((host, port))
                connected = True
            except BluetoothError:
                pass

        # then fire the ready event
        self.ready.set()

    def send(self, data):
        try:
            self.socket.send(data)
        except BluetoothError as error:
            # Meh, somethings gone wrong, try re-establishing link
            if self.socket is not None:
                self.socket.close()
                self.socket = None
            self.connect()

    def close(self):
        self.socket.close()
