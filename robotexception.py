class RobotStopException(Exception):
    """
    The simplest possible subclass of Exception, we can raise this if we want
    to stop the robot for any reason. Creating a custom exception like this
    makes the code more readable later.
    """
    pass
