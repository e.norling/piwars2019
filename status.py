import threading
import time
import blinkt

blinkt.set_clear_on_exit()

# Modes to display
STOPPED = 0
RUNNING = 1
LINE = 2
MAZE = 3
RAINBOW = 4
AUTONOMOUS = 5

class RobotStatus(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

        self.status = STOPPED
        self.mode = None
        self.start()

    def run(self):
        while True:
            time.sleep(0.1)

    def updateDisplay(self):
        if self.status == STOPPED:
            if self.mode is None:
                blinkt.set_all(255, 0, 0)
            elif self.mode == LINE:
                blinkt.set_all(0, 0, 255)
            elif self.mode == MAZE:
                self.maze()
            elif self.mode == RAINBOW:
                self.rainbow()
        else:
            blinkt.set_all(0, 255, 0)
        blinkt.show()

    def setStatus(self, status):
        self.status = status
        self.updateDisplay()

    def setMode(self, mode):
        self.mode = mode
        self.updateDisplay()

    def rainbow(self):
        """Draw rainbow that uniformly distributes itself across all pixels."""
        blinkt.set_pixel(0, 148, 0, 211) # violet
        blinkt.set_pixel(1, 75, 0, 130)  # indigo
        blinkt.set_pixel(2, 0, 0, 255)   # blue
        blinkt.set_pixel(3, 0, 255, 255) # light blue
        blinkt.set_pixel(4, 0, 255, 0)   # green
        blinkt.set_pixel(5, 255, 255, 0) # yellow
        blinkt.set_pixel(6, 255, 127, 0) # orange
        blinkt.set_pixel(7, 255, 0, 0)   # red

    def maze(self):
        for i in (0,1,2,3):
            blinkt.set_pixel(2*i, 255, 255, 0)      # yellow
            blinkt.set_pixel(2*i+1, 255, 127, 0)    # orange

# Main program logic follows:
if __name__ == '__main__':
    # Create NeoPixel object with appropriate configuration.
    status = RobotStatus()

    while True:
        print('line')
        status.setMode(LINE)
        time.sleep(2)
        print('maze')
        status.setMode(MAZE)
        time.sleep(2)
        print('rainbow')
        status.setMode(RAINBOW)
        time.sleep(2)
        print('running')
        status.setStatus(RUNNING)
        time.sleep(2)
        print('stopped')
        status.setStatus(STOPPED)
        status.setMode(None)
        time.sleep(2)
