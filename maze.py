import time
from threading import Event, Thread
import math
import sys
import VL53L0X
from util import IntervalCheck
from bluetooth import *
import colorsys
from rgbmatrix5x5 import RGBMatrix5x5
from comms import Comms
from headlights import Headlights
import ledshim

import RPi.GPIO as GPIO
GPIO.setwarnings(False)

# If the robot veers to one side when both these values are at 1,
# decrease the value of the other side throttle (e.g. to 0.95)
# to even them out
LEFT_THROTTLE = 1
RIGHT_THROTTLE = 1

# Robot dimensions
ROBOT_WIDTH = 145   # width between left and right mid sensors of robot
ROBOT_MID = 145/2
SENSOR_TO_BACK = 93        # spacing between 2 sensors on one side (mm)
SENSOR_TO_FRONT = 45    # Distance from front side sensor to front of tracks (mm)
SIDE_DIFF = 30

# Course dimensions
MAZE_WIDTH = 600
FRONT_CLEARANCE = 300
BIG_FRONT= MAZE_WIDTH-100
MIN_DIST = 120 # Don't want the robot any closer to the wall than this
MAX_DIST = MIN_DIST+ROBOT_WIDTH # Don't want the robot any further from the wall than this
CRITICAL_DISTANCE = 60
MIN_ANGLE = math.pi/30 # Min angle to worry about (5 degrees, in rad)

SLOW_ZONE = 500
LIMITER = 0.6

MARGIN = 120

MIN_POWER = 50      # Start from this speed (%)
MAX_POWER = 100     # Never go faster than this speed (%)
SPIN_POWER = MIN_POWER

SPIN_INC = 0.1

TURN_TIME = 0.02    # Time to perform a short turn on the spot

PI_ON_2 = math.pi/2

INC = 0.05
EPSILON = 10
MAX_ANGLE = math.pi/6 # 30 degress (in radians)

MAX_READING = 8190  # Value returned by sensors when nothing in range

CLOSER = 1
FURTHER = 2
STRAIGHT = 0

# GPIO pins for Sensor shutdown pins
sensor_front_shutdown = 21
sensor_left_shutdown = 26
sensor_left_back_shutdown = 16
sensor_right_shutdown = 19
sensor_right_back_shutdown = 20

def calc_angle_and_front(front_sensor,back_sensor):
    direction = STRAIGHT
    diff = front_sensor - back_sensor
    angle = 0
    front = front_sensor
    if front_sensor > MAZE_WIDTH or back_sensor > MAZE_WIDTH:
        # meaningless at an opening point
        return -1,-1, -1
    
    if diff < -EPSILON:
        #print('Pointing closer')
        direction = CLOSER
        angle = math.atan2(back_sensor-front_sensor,SENSOR_TO_BACK)
        front = (front_sensor*SENSOR_TO_BACK/(back_sensor-front_sensor)-SENSOR_TO_FRONT)*math.sin(angle)
    elif diff > EPSILON:
        #print('Pointing further')
        direction = FURTHER
        angle = math.atan2(front_sensor-back_sensor,SENSOR_TO_BACK)
        front = (front_sensor*SENSOR_TO_BACK/(front_sensor-back_sensor)+SENSOR_TO_FRONT)*math.sin(angle)
    return direction, angle, front

class Brains():

    def __init__(self, comms, ready=None):
        self.ready = ready # ready should not be set until tof sensors are setup
        self.pw_left = 0
        self.pw_right = 0
        self.running = False
        self.following_left = True
        self.apply_limiter = False
        self.comms = comms
        self.lights = Headlights()
        self.lights.start()

    def setup_tof(self):
        # Setup GPIO for shutdown pins on each VL53L0X
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(sensor_front_shutdown, GPIO.OUT)
        GPIO.setup(sensor_left_shutdown, GPIO.OUT)
        GPIO.setup(sensor_right_shutdown, GPIO.OUT)
        GPIO.setup(sensor_right_back_shutdown, GPIO.OUT)
        GPIO.setup(sensor_left_back_shutdown, GPIO.OUT)

        # Set all shutdown pins low to turn off each VL53L0X
        GPIO.output(sensor_front_shutdown, GPIO.LOW)
        GPIO.output(sensor_left_shutdown, GPIO.LOW)
        GPIO.output(sensor_right_shutdown, GPIO.LOW)
        GPIO.output(sensor_right_back_shutdown, GPIO.LOW)
        GPIO.output(sensor_left_back_shutdown, GPIO.LOW)

        # Keep all low for 500 ms or so to make sure they reset
        time.sleep(0.50)

        # Create one object per VL53L0X passing the address to give to
        # each.
        self.front_tof = VL53L0X.VL53L0X(address=0x2B)
        self.left_tof = VL53L0X.VL53L0X(address=0x2D)
        self.right_tof = VL53L0X.VL53L0X(address=0x2F)
        self.right_back_tof = VL53L0X.VL53L0X(address=0x31)
        self.left_back_tof = VL53L0X.VL53L0X(address=0x33)

        # Set shutdown pin high for the first VL53L0X then 
        # call to start ranging 
        GPIO.output(sensor_front_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.front_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

        GPIO.output(sensor_left_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.left_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

        GPIO.output(sensor_right_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.right_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

        GPIO.output(sensor_right_back_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.right_back_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

        GPIO.output(sensor_left_back_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.left_back_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

        self.timing = self.right_back_tof.get_timing()
        if (self.timing < 20000):
            self.timing = 20000
        #print ("Timing %d ms" % (self.timing))
        self.ready.set()

    def get_distances(self):
        # Get readings from distance sensors
        self.left_distance = self.left_tof.get_distance()
        self.front_distance = self.front_tof.get_distance()
        self.right_distance = self.right_tof.get_distance()
        self.right_back_distance = self.right_back_tof.get_distance()
        self.left_back_distance = self.left_back_tof.get_distance()
        while self.left_distance < 0 and self.right_distance < 0 and self.right_back_distance < 0 and self.front_distance < 0 and self.left_back_distance < 0:
            print('All readings shot')
            time.sleep(0.05)
            self.left_distance = self.left_tof.get_distance()
            self.front_distance = self.front_tof.get_distance()
            self.right_distance = self.right_tof.get_distance()
            self.right_back_distance = self.right_back_tof.get_distance()
            self.left_back_distance = self.left_back_tof.get_distance()
        #print('f: %dmm l: %dmm lb %dmm r: %dmm rb: %dmm' % (self.front_distance, self.left_distance, self.left_back_distance, self.right_distance, self.right_back_distance))

    def in_corridor(self, width):
        """
        Returns true if we are in a corridor of width as specfied
        Fairly crude check: just ensures that there isn't a space more than the width
        on either side of the robot
        :param int width
            The width of the corridor (mm)
        """
        return self.left_distance < width and self.right_distance < width

    def wall_on_left(self, width):
        """
        Returns true if we have a wall on left within width mm
        :param int width
            The width to check (mm)
        """
        return self.left_distance < width and self.left_back_distance < width

    def wall_on_right(self, width):
        """
        Returns true if we have a wall on right within width mm
        :param int width
            The width to check (mm)
        """
        return self.right_distance < width and self.right_back_distance < width

    def wall_in_front(self):
        if self.front_distance < FRONT_CLEARANCE:
            # Take another reading just to make sure it wasn't the robot rocking
            self.front_distance = self.front_tof.get_distance()
            if self.front_distance < FRONT_CLEARANCE:
                return True
        return False

    def is_stopped(self):
        return self.pw_left == 0 and self.pw_right == 0

    def stop_motors(self):
        self.set_motors(0,0)

    def set_motors(self, left, right):
        # Keep within allowed bounds for speed
        #print('Trying to set speeds to %f %f'%(left, right))
        
        left = min(left, MAX_POWER)
        right = min(right, MAX_POWER)

        if self.apply_limiter and left > 0 and right > 0:
            pw_factor = abs(LIMITER*MIN_POWER/max(left,right))
            left = left*pw_factor
            right = right*pw_factor

        self.pw_left = left
        self.pw_right = right

        motor_l = int(self.pw_left*LEFT_THROTTLE)
        motor_r = int(self.pw_right*RIGHT_THROTTLE)
        self.comms.send("%d %d\n" % (motor_l, motor_r))
        #print("motors: %d %d\n"%(motor_l, motor_r))

    def increase_motors(self):
        if self.pw_left == 0 or self.pw_right == 0:
            left = MIN_POWER
            right = MIN_POWER
        else:
            left = self.pw_left * (1+INC)
            right = self.pw_right * (1+INC)
        self.set_motors(left, right)

    def reduce_left(self):
        #print('In reduce left, with powers %f %f'%(self.pw_left, self.pw_right))
        right = max(self.pw_right,MIN_POWER)
        if self.pw_right < self.pw_left:
            right = self.pw_left
        elif self.pw_left == 0:
            left = MIN_POWER
        left = self.pw_left * (1-INC)
        self.set_motors(left, right)

    def reduce_right(self):
        #print('In reduce right, with powers %f %f'%(self.pw_left, self.pw_right))
        left = max(self.pw_left, MIN_POWER)
        if self.pw_left < self.pw_right:
            left = self.pw_right
        elif self.pw_right == 0:
            right = MIN_POWER
        right = self.pw_right * (1-INC)
        self.set_motors(left, right)
        
    def turn_right(self):
        self.set_motors(MIN_POWER, 0)
        
    def turn_left(self):
        self.set_motors(0, MIN_POWER)

    def straighten_right(self):
        if self.pw_left == 0 or self.pw_right == 0:
            self.pw_left = MIN_POWER
            self.pw_right = MIN_POWER
        if self.right_distance > self.right_back_distance:
            # currently heading left
            self.reduce_right()
        else:
            # currently heading right
            self.reduce_left()

    def straighten_left(self):
        if self.pw_left == 0 or self.pw_right == 0:
            self.pw_left = MIN_POWER
            self.pw_right = MIN_POWER
        if self.left_distance > self.left_back_distance:
            # currently heading right
            self.reduce_left()
        else:
            # currently heading left
            self.reduce_right()

    def spin_right(self):
        padding = IntervalCheck(interval=self.timing/1000000.0)
        self.set_motors(SPIN_POWER,-SPIN_POWER)
        aligned = False
        #print("Spinning right; left sensors are %d %d"%(self.left_distance,self.left_back_distance))

        while not aligned:
            time.sleep(SPIN_INC)
            self.stop_motors()
            self.get_distances()
            direction, angle, front_left = calc_angle_and_front(self.left_distance, self.left_back_distance)
            aligned = self.front_distance > FRONT_CLEARANCE and direction != CLOSER
            if not aligned:
                self.set_motors(SPIN_POWER,-SPIN_POWER)

    def spin_left(self):
        spin = SPIN_POWER-5

        padding = IntervalCheck(interval=self.timing/1000000.0)
        self.set_motors(-spin,spin)
        aligned = False
        #print("Spinning left; right sensors are %d %d"%(self.right_distance, self.right_back_distance))

        while not aligned:
            time.sleep(SPIN_INC)
            self.stop_motors()
            self.get_distances()
            direction, angle, front_right = calc_angle_and_front(self.right_distance, self.right_back_distance)
            aligned = self.front_distance > FRONT_CLEARANCE and direction != CLOSER
            if not aligned:
                self.set_motors(-spin,spin)

    def go_forward(self):
        self.set_motors(MIN_POWER, MIN_POWER)
  
    def run_maze(self):
        padding = IntervalCheck(interval=self.timing/1000000.0)

        while True:
                #print("Next main loop:")
                self.get_distances()
                if self.front_distance < SLOW_ZONE:
                    self.apply_limiter = True
                else:
                    self.apply_limiter = False
       
                if self.wall_in_front():
                    self.apply_limiter = False
                    if self.right_distance > self.left_distance:
                        self.lights.indicator(self.lights.right_light)
                        self.spin_right()
                    else:
                        self.lights.indicator(self.lights.left_light)
                        self.spin_left()
                else:
                    self.lights.off()
                    right_wall = self.wall_on_right(MAZE_WIDTH)
                    left_wall = self.wall_on_left(MAZE_WIDTH)
                    if right_wall:
                        # line up with this wall
                        direction, angle, front_right = calc_angle_and_front(self.right_distance, self.right_back_distance)
                    if left_wall:
                        # line up with this wall
                        direction2, angle2, front_left = calc_angle_and_front(self.left_distance, self.left_back_distance)
                    if left_wall and right_wall:
                        # Work with closer wall
                        if front_left > front_right:
                            left_wall = False
                        else:
                            right_wall = False

                    if right_wall:
                        # line up with this wall
                        #print("dir: %d, angle %.2f, front right %.2f"%(direction, angle, front_right))
                        if (direction == CLOSER or angle < MIN_ANGLE) and front_right < CRITICAL_DISTANCE:
                            #print('Ouch way too close on right')
                            self.comms.send("%d %d\n" % (int(-MIN_POWER*LEFT_THROTTLE), int(MIN_POWER*RIGHT_THROTTLE)))
                            time.sleep(TURN_TIME)
                            self.stop_motors()
                        elif (direction == CLOSER or angle < MIN_ANGLE) and front_right < MIN_DIST:
                            #print('Too close to right wall')
                            self.turn_left()
                        elif (direction == FURTHER or angle < MIN_ANGLE) and front_right > MAX_DIST:
                            self.turn_right()
                        else:
                            self.increase_motors()

                    elif left_wall:
                        #print("dir: %d, angle %.2f, front right %.2f"%(direction, angle, front_left))
                        if (direction2 == CLOSER or angle2 < MIN_ANGLE) and front_left < CRITICAL_DISTANCE:
                            #print('Ouch way too close on left')
                            self.comms.send("%d %d\n" % (int(MIN_POWER*LEFT_THROTTLE), int(-MIN_POWER*RIGHT_THROTTLE)))
                            time.sleep(TURN_TIME)
                            self.stop_motors()
                        elif (direction2 == CLOSER or angle2 < MIN_ANGLE) and front_left < MIN_DIST:
                            #print('Too close to right wall')
                            self.turn_right()
                        elif (direction2 == FURTHER or angle2 < MIN_ANGLE) and front_left > MAX_DIST:
                            self.turn_left()
                        else:
                            self.increase_motors()
                    else:
                        # must be at a sticky out bit, just go forward
                        self.go_forward()
                    
try:
    ledshim.set_clear_on_exit(False)
    ledshim.set_all(0xff, 0, 0)
    ledshim.show()
    comms_ready = Event()
    comms = Comms(comms_ready)

    # configure & start threa
    thread = Thread(target=comms.connect)
    thread.start()

    # block until comms ready
    comms_ready.wait()
    
    # now we can safely use controller
    #print("Ready")
    brains_ready = Event()
    brains = Brains(comms, brains_ready)

    thread = Thread(target=brains.setup_tof)
    thread.start()

    # block until ready to run (tof sensors set up)
    brains_ready.wait()
    ledshim.set_all(184,134,11)
    ledshim.show()
    input("READY: Press <enter> to start")
    ledshim.set_all(0, 0xff, 0)
    ledshim.show()
    # Now run the maze
    brains.run_maze()
    # GFX HAT code needs to go here...

except KeyboardInterrupt:
    comms.send("0 0\n") # Stop the robot before exiting
    comms.close()
    ledshim.set_all(0xff,0,0)
    ledshim.show()
    GPIO.cleanup()
