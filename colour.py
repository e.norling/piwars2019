import time
from threading import Event, Thread
import math
import VL53L0X
from picamera.array import PiRGBArray
from picamera import PiCamera
import cv2
import numpy as np
from util import IntervalCheck
from comms import Comms
from headlights import Headlights
import ledshim
import colorsys


import RPi.GPIO as GPIO
GPIO.setwarnings(False)

# If the robot veers to one side when both these values are at 1,
# decrease the value of the other side throttle (e.g. to 0.95)
# to even them out
LEFT_THROTTLE = 1
RIGHT_THROTTLE = 1

MIN_POWER = 40      # Start from this speed (%)
REGULAR_TURN_POWER = 60
MAX_POWER = 80     # Never go faster than this speed (%)
INC = 0.01          # Amount to increase/decrease speed by in one step

TOLERANCE = 10  # tolerance (in pixel count) for position

# Constants for image processing
IMG_WIDTH = 320
IMG_HEIGHT = 240
MID_IMAGE = IMG_WIDTH/2
IMG_EPS = 40
RAINBOW_VISIT_ZONE = 240
RAINBOW_TURN_DISTANCE = 400
TINY_TURN = 0.04

FRONT_CLEARANCE = 250

# Bounds for different colours (as (lower,upper) bounds for HSV values)
# If you tune with colour tuner, it will spit out the values when you hit ESC
green_minH = 50
green_maxH = 80
yellow_minH = 20
yellow_maxH = 45
blue_minH = 90
blue_maxH = 109
red1_minH = 0
red1_maxH = 10
red2_minH = 160
red2_maxH = 180
minS = 126
maxS = 255
minV = 70
maxV = 255
yellow=(np.array([yellow_minH,minS,minV]),np.array([yellow_maxH,maxS,maxV]))
green=(np.array([green_minH,minS,minV]),np.array([green_maxH,maxS,maxV]))
blue=(np.array([blue_minH,minS,minV]),np.array([blue_maxH,maxS,maxV]))
red1=(np.array([red1_minH,minS,minV]),np.array([red1_maxH,maxS,maxV]))
red2=(np.array([red2_minH,minS,minV]),np.array([red2_maxH,maxS,maxV]))

RED = 0
BLUE = 1
YELLOW = 2
GREEN = 3
COLOUR_TURN_TIME = 0.1
COLOUR_VISIT_ZONE = 100
COLOUR_TURN_DISTANCE = 300
MIN_BLOB_SIZE = 400

kernal = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7, 7))

# GPIO pins for ToF sensor shutdown pins
sensor_front_shutdown = 21
sensor_left_shutdown = 26
sensor_left_back_shutdown = 16
sensor_right_shutdown = 19
sensor_right_back_shutdown = 20

class Brains():

    def __init__(self, comms, ready=None):
        self.ready = ready
        self.pw_left = 0
        self.pw_right = 0
        self.comms = comms
        self.camera = PiCamera()
        self.camera.resolution = (IMG_WIDTH, IMG_HEIGHT)
        self.camera.framerate = 32
        self.rawCapture = PiRGBArray(self.camera, size=(IMG_WIDTH, IMG_HEIGHT))
        self.lights = Headlights()
        self.lights.off()

    def off(self):
        self.lights.off()

    def setup_tof(self):
        # Setup GPIO for shutdown pins on each VL53L0X
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(sensor_front_shutdown, GPIO.OUT)
        GPIO.setup(sensor_left_shutdown, GPIO.OUT)
        GPIO.setup(sensor_right_shutdown, GPIO.OUT)
        GPIO.setup(sensor_right_back_shutdown, GPIO.OUT)
        GPIO.setup(sensor_left_back_shutdown, GPIO.OUT)

        # Set all shutdown pins low to turn off each VL53L0X
        GPIO.output(sensor_front_shutdown, GPIO.LOW)
        GPIO.output(sensor_left_shutdown, GPIO.LOW)
        GPIO.output(sensor_right_shutdown, GPIO.LOW)
        GPIO.output(sensor_right_back_shutdown, GPIO.LOW)
        GPIO.output(sensor_left_back_shutdown, GPIO.LOW)

        # Keep all low for 500 ms or so to make sure they reset
        time.sleep(0.50)

        # Create one object per VL53L0X passing the address to give to
        # each.
        self.front_tof = VL53L0X.VL53L0X(address=0x2B)
        self.left_tof = VL53L0X.VL53L0X(address=0x2D)
        self.right_tof = VL53L0X.VL53L0X(address=0x2F)
        self.right_back_tof = VL53L0X.VL53L0X(address=0x31)
        self.left_back_tof = VL53L0X.VL53L0X(address=0x33)

        # Set shutdown pin high for the first VL53L0X then 
        # call to start ranging 
        GPIO.output(sensor_front_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.front_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

        GPIO.output(sensor_left_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.left_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

        GPIO.output(sensor_right_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.right_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

        GPIO.output(sensor_right_back_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.right_back_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

        GPIO.output(sensor_left_back_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.left_back_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

        self.timing = self.right_back_tof.get_timing()
        if (self.timing < 20000):
            self.timing = 20000
        print ("Timing %d ms" % (self.timing))
        self.ready.set()

    def get_distances(self):
        # Get readings from distance sensors
        self.left_distance = self.left_tof.get_distance()
        self.front_distance = self.front_tof.get_distance()
        self.right_distance = self.right_tof.get_distance()
        self.right_back_distance = self.right_back_tof.get_distance()
        self.left_back_distance = self.left_back_tof.get_distance()
        while self.left_distance < 0 and self.right_distance < 0 and self.right_back_distance < 0 and self.front_distance < 0 and self.left_back_distance < 0:
            print('All readings shot')
            time.sleep(0.05)
            self.left_distance = self.left_tof.get_distance()
            self.front_distance = self.front_tof.get_distance()
            self.right_distance = self.right_tof.get_distance()
            self.right_back_distance = self.right_back_tof.get_distance()
            self.left_back_distance = self.left_back_tof.get_distance()
        #print('f: %dmm l: %dmm lb %dmm r: %dmm rb: %dmm' % (self.front_distance, self.left_distance, self.left_back_distance, self.right_distance, self.right_back_distance))

    def wall_in_front(self, distance = FRONT_CLEARANCE):
        #print("Front distance is %d"%(self.front_distance))

        if self.front_distance < distance:
            # Take another reading just to make sure it wasn't the robot rocking
            self.front_distance = self.front_tof.get_distance()
            if self.front_distance < distance:
                return True
        return False
    
    def is_stopped(self):
        return self.pw_left == 0 and self.pw_right == 0

    def stop_motors(self):
        self.set_motors(0, 0)

    def set_motors(self, left, right):
        # Keep within allowed bounds for speed
        #print('Trying to set speeds to %f %f'%(left, right))
        
        left = min(left, MAX_POWER)
        right = min(right, MAX_POWER)

        self.pw_left = left
        self.pw_right = right

        motor_l = int(self.pw_left*LEFT_THROTTLE)
        motor_r = int(self.pw_right*RIGHT_THROTTLE)
        self.comms.send("%d %d\n" % (motor_l, motor_r))
        #print("motors: %d %d\n"%(motor_l, motor_r))

    def increase_motors(self):
        if self.pw_left == 0 or self.pw_right == 0:
            left = MIN_POWER
            right = MIN_POWER
        else:
            left = self.pw_left * (1+INC)
            right = self.pw_right * (1+INC)
        self.set_motors(left, right)

    def decrease_motors(self): 
        left = self.pw_left * (1-INC)
        right = self.pw_right * (1-INC)        #if MIN_POWER < left and MIN_POWER < right:
        self.set_motors(left, right)

    def reduce_left(self):
        #print('In reduce left, with powers %f %f'%(self.pw_left, self.pw_right))
        right = self.pw_right
        if self.pw_right < self.pw_left:
            right = self.pw_left
        elif self.pw_left == 0:
            left = MIN_POWER
        left = self.pw_left * (1-INC)
        self.set_motors(left, right)

    def reduce_right(self):
        #print('In reduce right, with powers %f %f'%(self.pw_left, self.pw_right))
        left = self.pw_left
        if self.pw_left < self.pw_right:
            left = self.pw_right
        elif self.pw_right == 0:
            right = MIN_POWER
        right = self.pw_right * (1-INC)
        self.set_motors(left, right)

    def turn_right(self):
        self.set_motors(MIN_POWER - 30, 0)
        
    def turn_left(self):
        self.set_motors(0, MIN_POWER - 30)

    def go_forward(self):
        self.set_motors(MIN_POWER, MIN_POWER)

    def go_backward(self):
        self.set_motors(-MIN_POWER, -MIN_POWER)

    def run_colour(self):
        self.goto_colour(RED)
        self.goto_colour(BLUE)
        self.goto_colour(YELLOW)
        self.goto_colour(GREEN)
        self.comms.send("0, 0")
        
        # Do some pretties now we've finished
        self.lights.rainbow()
        spacing = 360.0 / 16.0
        hue = 0

        while True:
            hue = int(time.time() * 100) % 360
            for x in range(ledshim.NUM_PIXELS):
                offset = x * spacing
                h = ((hue + offset) % 360) / 360.0
                r, g, b = [int(c * 255) for c in colorsys.hsv_to_rgb(h, 1.0, 1.0)]
                ledshim.set_pixel(x, r, g, b)

            ledshim.show()
            time.sleep(0.0001)



    def goto_colour(self, colour):
        print("Going to color %d"%(colour))
        # If we're too close to a wall, back off
        padding = IntervalCheck(interval=self.timing/1000000.0)
        self.get_distances()
        tooClose = self.wall_in_front(distance=COLOUR_TURN_DISTANCE)
        self.go_backward()
        while tooClose:
            with padding:
                self.get_distances()
                tooClose = self.wall_in_front(distance=COLOUR_TURN_DISTANCE)
        self.stop_motors()
        
        self.find_colour(colour)
        print("Found colour %d"%(colour))
        padding = IntervalCheck(interval=self.timing/1000000.0)
        self.get_distances()
        closeEnough = self.wall_in_front(distance = COLOUR_VISIT_ZONE)
        if not closeEnough:
            self.go_forward()
        while not closeEnough:
            with padding:
                self.get_distances()
                closeEnough = self.wall_in_front(distance = COLOUR_VISIT_ZONE)
        
        self.stop_motors()
        print("Reached colour %d"%(colour))

    def find_colour(self, colour):
        conts = []
        padding = IntervalCheck(interval=self.timing/1000000.0)
        self.get_distances()
        while True:
            with padding:
                # convert BGR to HSV
                self.rawCapture.truncate(0)
                self.camera.capture(self.rawCapture, format="bgr", use_video_port=True)
                frame = self.rawCapture.array
                frameBGR = cv2.GaussianBlur(frame, (7, 7), 0)
                frameBGR = cv2.medianBlur(frameBGR, 7)
                hsv = cv2.cvtColor(frameBGR, cv2.COLOR_BGR2HSV)

                # Get the mask for the colour we're currently looking for
                if colour == GREEN:
                    green_mask = cv2.inRange(hsv, green[0], green[1])
                    green_mask = cv2.morphologyEx(green_mask, cv2.MORPH_CLOSE, kernal)
                    green_mask = cv2.morphologyEx(green_mask, cv2.MORPH_OPEN, kernal)
                    _,conts,h=cv2.findContours(green_mask.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
                elif colour == RED:
                    red_mask = cv2.inRange(hsv, red1[0], red1[1])+cv2.inRange(hsv, red2[0], red2[1])
                    red_mask = cv2.morphologyEx(red_mask, cv2.MORPH_CLOSE, kernal)
                    red_mask = cv2.morphologyEx(red_mask, cv2.MORPH_OPEN, kernal)
                    _,conts,h=cv2.findContours(red_mask.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
                elif colour == BLUE:
                    blue_mask = cv2.inRange(hsv, blue[0], blue[1])
                    blue_mask = cv2.morphologyEx(blue_mask, cv2.MORPH_CLOSE, kernal)
                    blue_mask = cv2.morphologyEx(blue_mask, cv2.MORPH_OPEN, kernal)
                    _,conts,h=cv2.findContours(blue_mask.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
                elif colour == YELLOW:
                    yellow_mask = cv2.inRange(hsv, yellow[0], yellow[1])
                    yellow_mask = cv2.morphologyEx(yellow_mask, cv2.MORPH_CLOSE, kernal)
                    yellow_mask = cv2.morphologyEx(yellow_mask, cv2.MORPH_OPEN, kernal)
                    _,conts,h=cv2.findContours(yellow_mask.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)

                # Now we've masked out everything but the colour of interest
                # and found the countour of the object(s) of that colour if there
                # are any

                self.get_distances()

                if len(conts) == 0: # Nothing found

                    # print('Padding interval is %f' %(padding.interval))
                    # Make sure we've got enough space to turn
                    tooClose = self.wall_in_front(distance=COLOUR_TURN_DISTANCE)
                    if tooClose:
                        self.go_backward()
                    else:
                        self.comms.send("%d %d\n" % (-REGULAR_TURN_POWER, REGULAR_TURN_POWER))
                else:
                    # Find centre of contour
                    M = cv2.moments(conts[0])
                    print("blob area is %d"%(M['m00']))
                    if (M['m00'] > MIN_BLOB_SIZE):
                        cX = int(M["m10"] / M["m00"])
                        cY = int(M["m01"] / M["m00"])

                        #print("Centre of colour is at %d"%(cX))
                        distance_from_centre = MID_IMAGE - (IMG_WIDTH - cX)

                        if abs(distance_from_centre) < IMG_EPS:
                            if (self.wall_in_front(distance=COLOUR_TURN_DISTANCE)):
                                return
                            else:
                                self.go_forward()
                        elif distance_from_centre < 0:
                            #print('Turn right')
                            self.comms.send("%d %d\n" % (int(MIN_POWER), int(-MIN_POWER)))
##                            time.sleep(TINY_TURN)
##                            self.stop_motors()
                        else:
                            #print('Turn left')
                            self.comms.send("%d %d\n" % (int(-MIN_POWER), int(MIN_POWER)))
##                            time.sleep(TINY_TURN)
##                            self.stop_motors()
        # END WHILE LOOP

        # Should only reach this point if we found something of the colour
        return

try:
    ledshim.set_clear_on_exit(False)
    ledshim.set_all(0xff, 0, 0)
    ledshim.show()
    comms_ready = Event()
    comms = Comms(comms_ready)
    #lights = Headlights()
    #lights.solid()

    # configure & start thread
    thread = Thread(target=comms.connect)
    thread.start()

    print("Setting up communications...")

    # block until comms ready
    comms_ready.wait()
    
    # now we can safely use controller
    print("Ready to communicate")
    brains_ready = Event()
    brains = Brains(comms, brains_ready)

    thread = Thread(target=brains.setup_tof)
    thread.start()

    # block until ready to run (tof sensors set up)
    brains_ready.wait()
    
    ledshim.set_all(184,134,11)
    ledshim.show()
    input("READY: Press <enter> to start")

    # Now run the maze
    ledshim.set_all(0, 0xff, 0)
    ledshim.show()
    brains.run_colour()

except KeyboardInterrupt:
    pass

finally:
    comms.send("0 0\n") # Stop the robot before exiting
    comms.close()
    ledshim.set_all(0xff,0,0)
    ledshim.show()
    brains.off()
    GPIO.cleanup()
