import time
from threading import Event, Thread
import math
from picamera.array import PiRGBArray
from picamera import PiCamera
import cv2
import numpy as np
from util import IntervalCheck
from comms import Comms
import ledshim

import RPi.GPIO as GPIO
GPIO.setwarnings(False)

# If the robot veers to one side when both these values are at 1,
# decrease the value of the other side throttle (e.g. to 0.95)
# to even them out
LEFT_THROTTLE = 1
RIGHT_THROTTLE = 1


MIN_POWER = 90      # Start from this speed (%)
MAX_POWER = 100     # Never go faster than this speed (%)
INC = 0.01          # Amount to increase/decrease speed by in one step
where = 1

TOLERANCE = 10  # tolerance (in pixel count) for position

# Constants for image processing
IMG_WIDTH = 320
IMG_HEIGHT = 240
MID_IMAGE = IMG_WIDTH/2
target_line = int(IMG_HEIGHT/10)

region_of_interest_vertices = [
    (0, 0),
    (IMG_WIDTH / 2, 2*IMG_HEIGHT / where),
    (IMG_WIDTH, 0),
]

def region_of_interest(img, vertices):
    # Define a blank matrix that matches the image height/width.
    mask = np.zeros_like(img)

    # Retrieve the number of color channels of the image.
    channel_count = img.shape[2]

    # Create a match color with the same color channel counts.
    match_mask_color = (255,) * channel_count
      
    # Fill inside the polygon
    cv2.fillPoly(mask, vertices, match_mask_color)
    
    # Returning the image only where mask pixels match
    masked_image = cv2.bitwise_and(img, mask)
    return masked_image

def find_mid_white(line):
    lower = -1
    upper = -1
    for index, pixel in enumerate(line):
        if lower == -1:
            if pixel == 255:
                lower = index
        elif upper == -1 and pixel == 0:
            upper = index

    if upper == -1:
        upper = IMG_WIDTH

    if lower == -1:
        # Didn't find any white line!
        return -1
    
    return lower + int((upper-lower)/2)            

class Brains():

    def __init__(self, comms, ready=None):
        self.ready = ready
        self.pw_left = 0
        self.pw_right = 0
        self.comms = comms
        self.camera = PiCamera()
        self.camera.resolution = (IMG_WIDTH, IMG_HEIGHT)
        self.camera.framerate = 32
        self.rawCapture = PiRGBArray(self.camera, size=(IMG_WIDTH, IMG_HEIGHT))

        self.ready.set()

    def is_stopped(self):
        return self.pw_left == 0 and self.pw_right == 0

    def stop_motors(self):
        self.set_motors(0, 0)

    def set_motors(self, left, right):
        # Keep within allowed bounds for speed
        #print('Trying to set speeds to %f %f'%(left, right))
        
        left = min(left, MAX_POWER)
        right = min(right, MAX_POWER)

        self.pw_left = left
        self.pw_right = right

        motor_l = int(self.pw_left*LEFT_THROTTLE)
        motor_r = int(self.pw_right*RIGHT_THROTTLE)
        self.comms.send("%d %d\n" % (motor_l, motor_r))
        #print("motors: %d %d\n"%(motor_l, motor_r))

    def increase_motors(self):
        if self.pw_left == 0 or self.pw_right == 0:
            left = MIN_POWER
            right = MIN_POWER
        else:
            left = self.pw_left * (1+INC)
            right = self.pw_right * (1+INC)
        self.set_motors(left, right)

    def decrease_motors(self): 
        left = self.pw_left * (1-INC)
        right = self.pw_right * (1-INC)        #if MIN_POWER < left and MIN_POWER < right:
        self.set_motors(left, right)

    def reduce_left(self):
        self.set_motors(MIN_POWER - 32, MIN_POWER)

    def reduce_right(self):
        self.set_motors(MIN_POWER, MIN_POWER - 32)

    def turn_right(self):
        self.set_motors(MIN_POWER - 30, 0)
        
    def turn_left(self):
        self.set_motors(0, MIN_POWER - 30)

    def go_forward(self):
        self.set_motors(MIN_POWER, MIN_POWER)

    def go_backward(self):
        self.set_motors(-MIN_POWER, -MIN_POWER)

    def run_line(self):
        while True:
            #convert BGR to HSV
            self.rawCapture.truncate(0)
            self.camera.capture(self.rawCapture, format="bgr", use_video_port=True)
            frame = self.rawCapture.array
            cropped_image = region_of_interest(frame,
                np.array([region_of_interest_vertices], np.int32),
                )
            #cv2.imshow('cropped',cropped_image)
            grey_image = cv2.cvtColor(cropped_image, cv2.COLOR_BGR2GRAY)
            #cv2.imshow('grey',grey_image)
            blurred = cv2.GaussianBlur(grey_image, (5, 5), 0)
            thresh = cv2.threshold(blurred, 130, 255, cv2.THRESH_BINARY)[1]
            #cv2.imshow('threshold',thresh)

            # set mid to the middle of the white line,
            # a certain distance in front of the robot
            mid = find_mid_white(thresh[target_line])
            print("Mid is at ", mid)
            # Now you need to do something depending on the value of mid
            # mid == 0 means detected line on far left,
            # mid == IMG_WIDTH means detected line on far right
            # mid == -1 means didn't find any white line

            if 150 < mid and mid < 171:
                self.increase_motors()
                self.go_forward()
                print("forward")

                where = 5

            else:

                where = 5
                
                if 210 < mid and mid < 321:
                    self.decrease_motors()
                    self.turn_left()
                    print("hard left")
            
                if 170 < mid and mid < 211:
                    self.decrease_motors()
                    self.reduce_left()
                    print("left")
        

                if 90 < mid and mid < 151:
                    self.decrease_motors()
                    self.reduce_right()
                    print("right")

                if 0 < mid and mid < 91:
                    self.decrease_motors()
                    self.turn_right()
                    print("hard right")

try:
    ledshim.set_clear_on_exit(False)
    ledshim.set_all(0xff, 0, 0)
    ledshim.show()
    comms_ready = Event()
    comms = Comms(comms_ready)

    # configure & start thread
    thread = Thread(target=comms.connect)
    thread.start()

    print("Setting up communications...")

    # block until comms ready
    comms_ready.wait()
    
    # now we can safely use controller
    print("Ready to communicate")
    brains_ready = Event()
    brains = Brains(comms, brains_ready)

    thread = Thread()
    thread.start()

    # block until ready to run (tof sensors set up)
    brains_ready.wait()

    ledshim.set_all(184,134,11)
    ledshim.show()
    input("READY: Press <enter> to start")
    # Now run the maze
    brains.run_line()

except KeyboardInterrupt:
    pass

finally:
    comms.send("0 0\n") # Stop the robot before exiting
    comms.close()
    ledshim.set_all(0xff,0,0)
    ledshim.show()
    GPIO.cleanup()
