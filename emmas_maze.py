import time
from threading import Event, Thread
import math
import sys
import VL53L0X
from util import IntervalCheck
from bluetooth import *
from comms import Comms

import RPi.GPIO as GPIO
GPIO.setwarnings(False)

# left motor goes faster than right, use this factor
# to even them out
LEFT_THROTTLE = 1
RIGHT_THROTTLE = 1

# Robot dimensions
ROBOT_WIDTH = 145   # width between left and right mid sensors of robot
ROBOT_MID = 145/2
SENSOR_TO_BACK = 93        # spacing between 2 sensors on one side (mm)
SENSOR_TO_FRONT = 45    # Distance from front side sensor to front of tracks (mm)
SIDE_DIFF = 30

# Course dimensions
CORRIDOR_WIDTH_MAZE = 360 # distance between the walls (mm)
MIN_DIST_MAZE = 80
MAX_DIST_MAZE = 140
CRITICAL_DISTANCE_MAZE = 60

SLOW_ZONE = 400
LIMITER = 0.75

MIN_ANGLE = math.pi/30 # Min angle to worry about (5 degrees, in rad)
FRONT_CLEARANCE = 250 # don't get closer to wall in front than this

MIN_POWER = 40      # Start from this speed (%)
MAX_POWER = 100     # Never go faster than this speed (%)
MAX_TURN = 25       # Max amount to add to min speed on outer wheel for a turn

TURN_TIME = 0.07    # Time to perform a short turn on the spot
RAINBOW_TURN_TIME = 0.1

PI_ON_2 = math.pi/2

INC = 0.05
EPSILON = 2       # Distance sensors tolerant to about 2mm
MAX_ANGLE = math.pi/6 # 30 degress (in radians)

MAX_READING = 8190  # Value returned by sensors when nothing in range

CLOSER = 1
FURTHER = 2
STRAIGHT = 0

# GPIO pins for Sensor shutdown pins
sensor_front_shutdown = 21
sensor_left_shutdown = 26
sensor_left_back_shutdown = 16
sensor_right_shutdown = 19
sensor_right_back_shutdown = 20

def calc_angle_and_front(mid,back):
    direction = STRAIGHT
    diff = mid - back
    angle = 0
    front = mid
    if diff < -EPSILON:
        print('Pointing closer')
        direction = CLOSER
        angle = math.atan2(back-mid,SENSOR_TO_BACK)
        front = (mid*SENSOR_TO_BACK/(back-mid)-SENSOR_TO_FRONT)*math.sin(angle)
    elif diff > EPSILON:
        print('Pointing further')
        direction = FURTHER
        angle = math.atan2(mid-back,SENSOR_TO_BACK)
        front = (mid*SENSOR_TO_BACK/(mid-back)+SENSOR_TO_FRONT)*math.sin(angle)
    return direction, angle, front

class Brains():

    def __init__(self, comms, ready=None):
        self.ready = ready # ready should not be set until tof sensors are setup
        self.pw_left = 0
        self.pw_right = 0
        self.running = False
        self.following_left = True
        self.comms = comms

    def get_distances(self):
        # Get readings from distance sensors
        self.left_distance = self.left_tof.get_distance()
        self.front_distance = self.front_tof.get_distance()
        self.right_distance = self.right_tof.get_distance()
        self.right_back_distance = self.right_back_tof.get_distance()
        self.left_back_distance = self.left_back_tof.get_distance()
        while self.left_distance < 0 and self.right_distance < 0 and self.right_back_distance < 0 and self.front_distance < 0 and self.left_back_distance < 0:
            print('All readings shot')
            time.sleep(0.05)
            self.left_distance = self.left_tof.get_distance()
            self.front_distance = self.front_tof.get_distance()
            self.right_distance = self.right_tof.get_distance()
            self.right_back_distance = self.right_back_tof.get_distance()
            self.left_back_distance = self.left_back_tof.get_distance()
        #print('f: %dmm l: %dmm lb %dmm r: %dmm rb: %dmm' % (self.front_distance, self.left_distance, self.left_back_distance, self.right_distance, self.right_back_distance))
            
    def setup_tof(self):
        # Setup GPIO for shutdown pins on each VL53L0X
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(sensor_front_shutdown, GPIO.OUT)
        GPIO.setup(sensor_left_shutdown, GPIO.OUT)
        GPIO.setup(sensor_right_shutdown, GPIO.OUT)
        GPIO.setup(sensor_right_back_shutdown, GPIO.OUT)
        GPIO.setup(sensor_left_back_shutdown, GPIO.OUT)

        # Set all shutdown pins low to turn off each VL53L0X
        GPIO.output(sensor_front_shutdown, GPIO.LOW)
        GPIO.output(sensor_left_shutdown, GPIO.LOW)
        GPIO.output(sensor_right_shutdown, GPIO.LOW)
        GPIO.output(sensor_right_back_shutdown, GPIO.LOW)
        GPIO.output(sensor_left_back_shutdown, GPIO.LOW)

        # Keep all low for 500 ms or so to make sure they reset
        time.sleep(0.50)

        # Create one object per VL53L0X passing the address to give to
        # each.
        self.front_tof = VL53L0X.VL53L0X(address=0x2B)
        self.left_tof = VL53L0X.VL53L0X(address=0x2D)
        self.right_tof = VL53L0X.VL53L0X(address=0x2F)
        self.right_back_tof = VL53L0X.VL53L0X(address=0x31)
        self.left_back_tof = VL53L0X.VL53L0X(address=0x33)

        # Set shutdown pin high for the first VL53L0X then 
        # call to start ranging 
        GPIO.output(sensor_front_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.front_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

        GPIO.output(sensor_left_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.left_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

        GPIO.output(sensor_right_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.right_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

        GPIO.output(sensor_right_back_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.right_back_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

        GPIO.output(sensor_left_back_shutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.left_back_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

        self.timing = self.right_back_tof.get_timing()
        if (self.timing < 20000):
            self.timing = 20000
        print ("Timing %d ms" % (self.timing))
        self.ready.set()
        self.get_distances()

    def in_corridor(self, width):
        """
        Returns true if we are in a corridor of width as specfied
        Fairly crude check: just ensures that there isn't a space more than the width
        on either side of the robot
        :param int width
            The width of the corridor (mm)
        """
        return self.left_distance < width and self.right_distance < width

    def wall_on_left(self, width):
        """
        Returns true if we have a wall on left within width mm
        :param int width
            The width to check (mm)
        """
        return self.left_distance < width and self.left_back_distance < width

    def wall_on_right(self, width):
        """
        Returns true if we have a wall on right within width mm
        :param int width
            The width to check (mm)
        """
        return self.right_distance < width and self.right_back_distance < width

    def wall_in_front(self, distance = FRONT_CLEARANCE):
        if self.front_distance < distance:
            # Take another reading just to make sure it wasn't the robot rocking
            self.front_distance = self.front_tof.get_distance()
            if self.front_distance < distance:
                return True
        return False

    def is_stopped(self):
        return self.pw_left == 0 and self.pw_right == 0

    def stop_motors(self):
        self.set_motors(0, 0)

    def set_motors(self, left, right):
        # Keep within allowed bounds for speed
        #print('Trying to set speeds to %f %f'%(left, right))
        
        left = min(left, MAX_POWER)
        right = min(right, MAX_POWER)

        if self.apply_limiter and left != 0 and right != 0:
            pw_factor = abs(LIMITER*MIN_POWER/max(left,right))
            left = left*pw_factor
            right = right*pw_factor

        self.pw_left = left
        self.pw_right = right

        motor_l = int(self.pw_left*LEFT_THROTTLE)
        motor_r = int(self.pw_right*RIGHT_THROTTLE)
        self.comms.send("%d %d\n" % (motor_l, motor_r))
        #print("motors: %d %d\n"%(motor_l, motor_r))

    def increase_motors(self):
        if self.pw_left == 0 or self.pw_right == 0:
            left = MIN_POWER
            right = MIN_POWER
        else:
            left = self.pw_left * (1+INC)
            right = self.pw_right * (1+INC)
        self.set_motors(left, right)

    def reduce_left(self):
        #print('In reduce left, with powers %f %f'%(self.pw_left, self.pw_right))
        right = self.pw_right
        if self.pw_right < self.pw_left:
            right = self.pw_left
        elif self.pw_left == 0:
            left = MIN_POWER
        left = self.pw_left * (1-INC)
        self.set_motors(left, right)

    def reduce_right(self):
        #print('In reduce right, with powers %f %f'%(self.pw_left, self.pw_right))
        left = self.pw_left
        if self.pw_left < self.pw_right:
            left = self.pw_right
        elif self.pw_right == 0:
            right = MIN_POWER
        right = self.pw_right * (1-INC)
        self.set_motors(left, right)

    def turn_right(self):
        self.set_motors(MIN_POWER, 0)
        
    def turn_left(self):
        self.set_motors(0, MIN_POWER)

    def go_forward(self):
        self.set_motors(MIN_POWER, MIN_POWER)

    def go_backward(self):
        self.set_motors(-MIN_POWER, -MIN_POWER)

    def keep_centered(self, width, min_dist, critical_distance):
        """
        Keep centered within a corridor of width as specified
        :param int width
        The width of the corridor (mm)
        :param int min_dist
        The minimum distance we should keep between robot and the wall
        :param int critical_distance
        The point at which we REALLY need to turn away from the wall
        """
        mid = (width - ROBOT_WIDTH)/2 - critical_distance

        d1, angle1, front_left = calc_angle_and_front(self.left_distance, self.left_back_distance)
        d2, angle2, front_right = calc_angle_and_front(self.right_distance, self.right_back_distance)

        if self.is_stopped():
            self.set_motors(MIN_POWER, MIN_POWER)

        #print('Power before adjustment %d %d'%(self.pw_left,self.pw_right))

        # First: make sure we're not way too close to one wall or the other
        # If we are, turn on the spot back towards the centre
        if (d1 == CLOSER or angle1 < MIN_ANGLE) and front_left < critical_distance:
            #print('Ouch way too close on left')
            self.comms.send("%d %d\n" % (int(MIN_POWER*LEFT_THROTTLE), int(-MIN_POWER*RIGHT_THROTTLE)))
            time.sleep(TURN_TIME)
            self.stop_motors()
            return
        if (d2 == CLOSER or angle2 < MIN_ANGLE) and front_right < critical_distance:
            #print('Ouch way too close on right')
            self.comms.send("%d %d\n" % (int(-MIN_POWER*LEFT_THROTTLE), int(MIN_POWER*RIGHT_THROTTLE)))
            time.sleep(TURN_TIME)
            self.stop_motors()
            return

        # Next check: are we too far to the left?
        # If so, turn right

        if (d1 == CLOSER or angle1 < MIN_ANGLE) and front_left < min_dist:
            #print('Too close to left wall')
            self.turn_right()

        # And for the other side
        if (d2 == CLOSER or angle2 < MIN_ANGLE) and front_right < min_dist:
            #print('Too close to right wall')
            self.turn_left()
            
        # If we get this far, should be either travelling straight in the comfort zone
        # in which case, just keep going faster
        if angle1 < MIN_ANGLE:
            #print('Heading straight in comfort zone, zoom zoom')
            self.increase_motors()
            return

        # Or else we should ease off on the turn
        #print('Heading in the right direction, easing off the turn')
        if d1 == CLOSER:
            self.reduce_right()
        else:
            self.reduce_left()

    def follow_left(self, min_dist, max_dist, critical_distance):
        """
        Follow a wall on the left
        :param int min_dist
        The minimum distance we should keep between robot and the wall
        :param int max_dist
        The maximum distance we should go from the wall
        :param int critical_distance
        The point at which we REALLY need to turn away from the wall
        """
        
        d1, angle1, front_left = calc_angle_and_front(self.left_distance, self.left_back_distance)
        d2, angle2, front_right = calc_angle_and_front(self.right_distance, self.right_back_distance)

        if self.is_stopped():
            self.set_motors(MIN_POWER, MIN_POWER)
            
        #print('Power before adjustment %d %d'%(self.pw_left,self.pw_right))

        # First: make sure we're not way too close to one wall or the other
        # If we are, turn on the spot back towards the centre
        if (angle1 < MIN_ANGLE or d1 == CLOSER) and front_left < critical_distance:
            #print('Ouch way too close on left')
            self.comms.send("%d %d\n" % (int(MIN_POWER*LEFT_THROTTLE), int(-MIN_POWER*RIGHT_THROTTLE)))
            time.sleep(TURN_TIME)
            self.stop_motors()
            return
        if (angle2 < MIN_ANGLE or d2 == CLOSER) and front_right < critical_distance:
            #print('Ouch way too close on right')
            self.comms.send("%d %d\n" % (int(-MIN_POWER*LEFT_THROTTLE), int(MIN_POWER*RIGHT_THROTTLE)))
            time.sleep(TURN_TIME)
            self.stop_motors()
            return

        # Next check: are we too far to the left or travelling left on the wrong side of the centre line?
        # If so, increase power on left motor
        if (angle1 < MIN_ANGLE or d1 == CLOSER) and front_left < min_dist:
            #print('Heading in wrong direction near left wall')
            # Too far to the left, turn back to straight
            self.turn_right()
            return

        # Similarly if too far to the right or travelling right on the wrong side of the centre line?
        # If so, increase power on the right motor
        if (angle1 < MIN_ANGLE or d1 == FURTHER) and front_left > max_dist:
            self.turn_left()
            return
            
        # If we get this far, should be either travelling straight in the comfort zone
        # in which case, just keep going faster
        if angle1 < MIN_ANGLE:
            #print('Heading straight in comfort zone, zoom zoom')
            self.increase_motors()
            return

        # Or else we should ease off on the turn
        #print('Heading in the right direction, easing off the turn')
        if d1 == CLOSER:
            self.reduce_right()
        else:
            self.reduce_left()

    def follow_right(self, min_dist, max_dist, critical_distance):
        """
        Follow a wall on the right
        :param int min_dist
        The minimum distance we should keep between robot and the wall
        :param int max_dist
        The maximum distance we should go from the wall
        :param int critical_distance
        The point at which we REALLY need to turn away from the wall
        """
        
        d1, angle1, front_left = calc_angle_and_front(self.left_distance, self.left_back_distance)
        d2, angle2, front_right = calc_angle_and_front(self.right_distance, self.right_back_distance)

        #print('Front left is %f, front right is %f'%(front_left,front_right))
        if self.is_stopped():
            self.set_motors(MIN_POWER, MIN_POWER)
            
        #print('Power before adjustment %d %d'%(self.pw_left,self.pw_right))

        # First: make sure we're not way too close to one wall or the other
        # If we are, turn on the spot back towards the centre
        if (angle2 < MIN_ANGLE or d2 == CLOSER) and front_right < critical_distance:
            #print('Ouch way too close on right')
            self.comms.send("%d %d\n" % (int(-MIN_POWER*LEFT_THROTTLE), int(MIN_POWER*RIGHT_THROTTLE)))
            time.sleep(TURN_TIME)
            self.stop_motors()
            return
        if (angle1 < MIN_ANGLE or d1 == CLOSER) and front_left < critical_distance:
            #print('Ouch way too far on right')
            self.comms.send("%d %d\n" % (int(MIN_POWER*LEFT_THROTTLE), int(-MIN_POWER*RIGHT_THROTTLE)))
            time.sleep(TURN_TIME)
            self.stop_motors()
            return

        # Next: check are we too far to the right
        # If so, increase power on right motor
        if (angle2 < MIN_ANGLE or d2 == CLOSER) and front_right < min_dist:
            self.turn_left()
            return
        # Or similar for the other side of the comfort zone
        if (angle2 < MIN_ANGLE or d2 == FURTHER) and front_right > max_dist:
            self.turn_right()
            return


        # if we get this far, should be either travelling straight in the comfort zone
        # in which case, just keep going faster
        if angle2 < MIN_ANGLE:
            #print('heading straight in comfort zone, zoom zoom')
            self.increase_motors()
            return

        # or else we should ease off on the turn
        #print('heading in the right direction, easing off the turn')
        if d2 == FURTHER:
            self.reduce_right()
        else:
            self.reduce_left()

    def maze_step(self):
        self.apply_limiter = False
        print('Front distance %d slow zone %d'%(self.front_distance, SLOW_ZONE))
        if self.front_distance < SLOW_ZONE:
            print('Should be limiting speed here')
            self.apply_limiter = True
        if self.following_left:
            if self.wall_in_front():
                print('Should turn right here')
                self.turn_right()
            elif self.wall_on_left(CORRIDOR_WIDTH_MAZE):
                print('Got a wall on the left')
                self.follow_left(MIN_DIST_MAZE, MAX_DIST_MAZE, CRITICAL_DISTANCE_MAZE)
            else:
                print('Sticky outy bit')
                if not self.wall_on_right(CORRIDOR_WIDTH_MAZE):
                    self.comms.send("%d %d\n" % (int(MIN_POWER*LEFT_THROTTLE), int(MIN_POWER*RIGHT_THROTTLE)))
                    time.sleep(TURN_TIME)
                    self.set_motors(0, 0)
                self.following_left = False
        else:
            if self.wall_in_front():
                print('Should turn left here')
                self.turn_left()
            elif self.wall_on_right(CORRIDOR_WIDTH_MAZE):
                print('Got a wall on the right')
                self.follow_right(MIN_DIST_MAZE, MAX_DIST_MAZE, CRITICAL_DISTANCE_MAZE)
            else:
                print('Sticky outy bit')
                if not self.wall_on_left(CORRIDOR_WIDTH_MAZE):
                    self.comms.send("%d %d\n" % (int(MIN_POWER*LEFT_THROTTLE), int(MIN_POWER*RIGHT_THROTTLE)))
                    time.sleep(TURN_TIME)
                    self.set_motors(0, 0)
                self.following_left = True

    def run_maze(self):
        padding = IntervalCheck(interval=self.timing/1000000.0)
        print('Padding interval is %f' %(padding.interval))
        while True:
            with padding:
                self.get_distances()
                self.maze_step()

try:
    comms_ready = Event()
    comms = Comms(comms_ready)

    # configure & start threa
    thread = Thread(target=comms.connect)
    thread.start()

    # block until comms ready
    comms_ready.wait()
    
    # now we can safely use controller
    print("Ready")
    brains_ready = Event()
    brains = Brains(comms, brains_ready)

    thread = Thread(target=brains.setup_tof)
    thread.start()

    # block until ready to run (tof sensors set up)
    brains_ready.wait()

    # Now run the maze
    brains.run_maze()
    # GFX HAT code needs to go here...

except KeyboardInterrupt:
    comms.send("0 0\n") # Stop the robot before exiting
    GPIO.cleanup()
