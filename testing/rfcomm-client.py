# file: rfcomm-client.py
# auth: Albert Huang <albert@csail.mit.edu>
# desc: simple demonstration of a client application that uses RFCOMM sockets
#       intended for use with rfcomm-server
#
# $Id: rfcomm-client.py 424 2006-08-24 03:35:54Z albert $
import threading
from bluetooth import *
import sys

addr = "B8:27:EB:1E:6A:88"

if len(sys.argv) < 2:
    print ("no device specified.  Searching all nearby bluetooth devices for")
    print ("the DriverServer service")
else:
    addr = sys.argv[1]
    print ("Searching for DriverServer on %s" % addr)

class Client(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        
        # search for the DriverServer service
        uuid = "5d122e1e-00fc-40af-82cc-bc71c6844a0f"
        service_matches = find_service( uuid = uuid, address = addr )

        if len(service_matches) == 0:
            print ("couldn't find the DriverServer service =(")
            sys.exit(0)

        first_match = service_matches[0]
        port = first_match["port"]
        name = first_match["name"]
        host = first_match["host"]

        print ("connecting to \"%s\" on %s" % (name, host))
    
        # Create the client socket
        self.sock=BluetoothSocket( RFCOMM )
        self.sock.connect((host, port))

        print ("connected.  type stuff")
        self.start()

    def run(self):
        while True:
            data = input()
            if len(data) == 0: break
            self.sock.send(data)

    def close(self):
        self.sock.close()

    def listen(self):
        data = self.sock.recv(1024)
        print("received", data)
        self.sock.send("Ooh I heard you")

try:
    comms = Client()
    while True:
        comms.listen()

except KeyboardInterrupt:
    comms.close()
