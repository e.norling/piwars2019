import cv2
import numpy as np
from picamera.array import PiRGBArray
from picamera import PiCamera

IMG_WIDTH = 320
IMG_HEIGHT = 240

camera = PiCamera()
camera.resolution = (IMG_WIDTH, IMG_HEIGHT)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(IMG_WIDTH, IMG_HEIGHT))

region_of_interest_vertices = [
    (0, 0),
    (IMG_WIDTH / 2, 2*IMG_HEIGHT / 3),
    (IMG_WIDTH, 0),
]

def region_of_interest(img, vertices):
    # Define a blank matrix that matches the image height/width.
    mask = np.zeros_like(img)

    # Retrieve the number of color channels of the image.
    channel_count = img.shape[2]

    # Create a match color with the same color channel counts.
    match_mask_color = (255,) * channel_count
      
    # Fill inside the polygon
    cv2.fillPoly(mask, vertices, match_mask_color)
    
    # Returning the image only where mask pixels match
    masked_image = cv2.bitwise_and(img, mask)
    return masked_image

def find_mid_white(line):
    lower = -1
    upper = -1
    for index, pixel in enumerate(line):
        if lower == -1:
            if pixel == 255:
                lower = index
        elif upper == -1 and pixel == 0:
            upper = index

    if upper == -1:
        upper = IMG_WIDTH

    return lower + int((upper-lower)/2)            

target_line = 80

while True:
    rawCapture.truncate(0)
    camera.capture(rawCapture, format="bgr", use_video_port=True)
    frame = rawCapture.array
    cv2.imshow('raw',frame)
    cropped_image = region_of_interest(frame,
        np.array([region_of_interest_vertices], np.int32),
        )
    cv2.imshow('cropped',cropped_image)
    grey_image = cv2.cvtColor(cropped_image, cv2.COLOR_BGR2GRAY)
    cv2.imshow('grey',grey_image)
    blurred = cv2.GaussianBlur(grey_image, (5, 5), 0)
    thresh = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]
    cv2.imshow('threshold',thresh)

    cv2.waitKey(0)
    line = thresh[target_line]
    #print(line, len(thresh[0]))
    mid = find_mid_white(line)
    #print(mid)
    img = cv2.drawMarker(frame, (mid,target_line),(0,0,255))
    cv2.imshow('detected',img)

    
cv2.destroyAllWindows()

