import cv2
import numpy as np
from picamera.array import PiRGBArray
from picamera import PiCamera

IMG_WIDTH = 320
IMG_HEIGHT = 240

camera = PiCamera()
camera.resolution = (IMG_WIDTH, IMG_HEIGHT)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(IMG_WIDTH, IMG_HEIGHT))

happy = False
while not happy:
    cv2.destroyAllWindows() # Get rid of images from last iteration
    print("Press any key when ready to capture region")
    ready = False
    while not ready:
        rawCapture.truncate(0)
        camera.capture(rawCapture, format="bgr", use_video_port=True)
        frame = rawCapture.array
        cv2.imshow('raw',frame)
        ready=(cv2.waitKey(31) != -1)

    cv2.destroyWindow('raw')
    # Select ROI
    print("Select region, then press any key when complete")
    fromCenter = False
    r = cv2.selectROI(frame, fromCenter)

    # Crop image
    imCrop = frame[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+r[2])]

    # Display cropped image
    cv2.imshow("cropped", imCrop)

    print("Save results?")
    happy=(cv2.waitKey(0) == 121) # Save if user types 'y'
    smallest = imCrop.min(axis=0).min(axis=0)
    biggest = imCrop.max(axis=0).max(axis=0)

    print(smallest, biggest)
    
cv2.destroyAllWindows()

