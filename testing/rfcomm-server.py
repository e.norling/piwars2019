# file: rfcomm-server.py
# auth: Albert Huang <albert@csail.mit.edu>
# desc: simple demonstration of a server application that uses RFCOMM sockets
#
# $Id: rfcomm-server.py 518 2007-08-10 07:20:07Z albert $
import threading
from bluetooth import *
import time
import random

class Server(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)

        self.server_sock=BluetoothSocket( RFCOMM )
        self.server_sock.bind(("",PORT_ANY))
        self.server_sock.listen(1)

        port = self.server_sock.getsockname()[1]

        uuid = "5d122e1e-00fc-40af-82cc-bc71c6844a0f"

        advertise_service( self.server_sock, "DriverServer",
                           service_id = uuid,
                           service_classes = [ uuid, SERIAL_PORT_CLASS ],
                           profiles = [ SERIAL_PORT_PROFILE ], 
                        )
                   
        print ("Waiting for connection on RFCOMM channel %d" % port)

        self.client_sock, client_info = self.server_sock.accept()
        print ("Accepted connection from ", client_info)
        self.start()

    def run(self):
        try:
            while True:
                data = self.client_sock.recv(1024)
                if len(data) == 0: break
                print ("received [%s]" % data)
        except IOError:
            pass

        print ("disconnected")

        self.client_sock.close()
        self.server_sock.close()
        print ("all done")

    def message(self, msg):
        self.client_sock.send(msg)

try:
    comms = Server()
    while True:
        time.sleep(random.randint(5,20))
        comms.message("Do you hear me?")
        print("Message sent")
        

except KeyboardInterrupt:
    print ('Finished')
