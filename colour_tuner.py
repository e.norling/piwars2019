"""
File: colour_tuner.py
based upon example provided in opencv-open-file-color-test.py, reused and modified under the MIT licencse
File: opencv-open-file-color-test.py
This Python 3 code is published in relation to the article below:
https://www.bluetin.io/opencv/opencv-color-detection-filtering-python/
Website:    www.bluetin.io
Author:     Mark Heywood
Date:     8/12/2017
Version     0.1.0
License:    MIT

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""
 
from __future__ import division
import cv2
import numpy as np
from picamera.array import PiRGBArray
from picamera import PiCamera
from headlights import Headlights
 
def nothing(*arg):
    pass
 
# Initial HSV GUI slider values to load on program start.
green_minH = 50
green_maxH = 89
yellow_minH = 20
yellow_maxH = 45
blue_minH = 90
blue_maxH = 109
red1_minH = 0
red1_maxH = 10
red2_minH = 160
red2_maxH = 180
minS = 70
maxS = 255
minV = 50
maxV = 255

cv2.namedWindow('green tracker')
cv2.namedWindow('blue tracker')
cv2.namedWindow('yellow tracker')
cv2.namedWindow('red tracker')
cv2.namedWindow('trackers')
# Lower range colour sliders.
cv2.createTrackbar('lowHue(g)', 'green tracker', green_minH, 180, nothing)
cv2.createTrackbar('highHue(g)', 'green tracker', green_maxH, 180, nothing)
cv2.createTrackbar('lowHue(b)', 'blue tracker', blue_minH, 180, nothing)
cv2.createTrackbar('highHue(b)', 'blue tracker', blue_maxH, 180, nothing)
cv2.createTrackbar('lowHue(y)', 'yellow tracker', yellow_minH, 180, nothing)
cv2.createTrackbar('highHue(y)', 'yellow tracker', yellow_maxH, 180, nothing)
cv2.createTrackbar('lowHue(r1)', 'red tracker', red1_minH, 180, nothing)
cv2.createTrackbar('highHue(r1)', 'red tracker', red1_maxH, 180, nothing)
cv2.createTrackbar('lowHue(r2)', 'red tracker', red2_minH, 180, nothing)
cv2.createTrackbar('highHue(r2)', 'red tracker', red2_maxH, 180, nothing)

cv2.createTrackbar('lowSat', 'trackers', minS, 255, nothing)
cv2.createTrackbar('highSat', 'trackers', maxS, 255, nothing)
cv2.createTrackbar('lowVal', 'trackers', minV, 255, nothing)
cv2.createTrackbar('highVal', 'trackers', maxV, 255, nothing)

# Constants for image processing
IMG_WIDTH = 320
IMG_HEIGHT = 240

camera = PiCamera()
camera.resolution = (IMG_WIDTH, IMG_HEIGHT)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(IMG_WIDTH, IMG_HEIGHT))

kernal = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7, 7))
lights = Headlights()
lights.off()
#lights.solid()
 
while True:
    rawCapture.truncate(0)
    camera.capture(rawCapture, format="bgr", use_video_port=True)
    frame = rawCapture.array

    # Get HSV values from the GUI sliders.
    lowHue_g = cv2.getTrackbarPos('lowHue(g)', 'green tracker')
    highHue_g = cv2.getTrackbarPos('highHue(g)', 'green tracker')
    lowHue_b = cv2.getTrackbarPos('lowHue(b)', 'blue tracker')
    highHue_b = cv2.getTrackbarPos('highHue(b)', 'blue tracker')
    lowHue_y = cv2.getTrackbarPos('lowHue(y)', 'yellow tracker')
    highHue_y = cv2.getTrackbarPos('highHue(y)', 'yellow tracker')
    lowHue_r1 = cv2.getTrackbarPos('lowHue(r1)', 'red tracker')
    highHue_r1 = cv2.getTrackbarPos('highHue(r1)', 'red tracker')
    lowHue_r2 = cv2.getTrackbarPos('lowHue(r2)', 'red tracker')
    highHue_r2 = cv2.getTrackbarPos('highHue(r2)', 'red tracker')
    lowSat = cv2.getTrackbarPos('lowSat', 'trackers')
    highSat = cv2.getTrackbarPos('highSat', 'trackers')
    lowVal = cv2.getTrackbarPos('lowVal', 'trackers')
    highVal = cv2.getTrackbarPos('highVal', 'trackers')
 
    # Show the original image.
    #cv2.imshow('frame', frame)
    
    # Blur methods available, comment or uncomment to try different blur methods.
    frameBGR = cv2.GaussianBlur(frame, (7, 7), 0)
    frameBGR = cv2.medianBlur(frameBGR, 7)
    #frameBGR = cv2.bilateralFilter(frameBGR, 15 ,75, 75)
    """kernal = np.ones((15, 15), np.float32)/255
    frameBGR = cv2.filter2D(frameBGR, -1, kernal)"""
    # Show blurred image.
    #cv2.imshow('blurred', frameBGR)
    # HSV (Hue, Saturation, Value).
    # Convert the frame to HSV colour model.
    hsv = cv2.cvtColor(frameBGR, cv2.COLOR_BGR2HSV)
    
    # HSV values to define a colour range.
    green_mask = cv2.inRange(hsv, np.array([lowHue_g,lowSat,lowVal]), np.array([highHue_g,highSat,highVal]))
    green_mask = cv2.morphologyEx(green_mask, cv2.MORPH_CLOSE, kernal)
    green_mask = cv2.morphologyEx(green_mask, cv2.MORPH_OPEN, kernal)
    
    # Put mask over top of the original image.
    green_result = cv2.bitwise_and(frame, frame, mask = green_mask)

    # HSV values to define a colour range.
    blue_mask = cv2.inRange(hsv, np.array([lowHue_b,lowSat,lowVal]), np.array([highHue_b,highSat,highVal]))
    blue_mask = cv2.morphologyEx(blue_mask, cv2.MORPH_CLOSE, kernal)
    blue_mask = cv2.morphologyEx(blue_mask, cv2.MORPH_OPEN, kernal)
    
    # Put mask over top of the original image.
    blue_result = cv2.bitwise_and(frame, frame, mask = blue_mask)

    # HSV values to define a colour range.
    yellow_mask = cv2.inRange(hsv, np.array([lowHue_y,lowSat,lowVal]), np.array([highHue_y,highSat,highVal]))
    yellow_mask = cv2.morphologyEx(yellow_mask, cv2.MORPH_CLOSE, kernal)
    yellow_mask = cv2.morphologyEx(yellow_mask, cv2.MORPH_OPEN, kernal)
    
    # Put mask over top of the original image.
    yellow_result = cv2.bitwise_and(frame, frame, mask = yellow_mask)

    # HSV values to define a colour range.
    red_mask = cv2.inRange(hsv, np.array([lowHue_r1,lowSat,lowVal]), np.array([highHue_r1,highSat,highVal]))+cv2.inRange(hsv, np.array([lowHue_r2,lowSat,lowVal]), np.array([highHue_r2,highSat,highVal]))
    red_mask = cv2.morphologyEx(red_mask, cv2.MORPH_CLOSE, kernal)
    red_mask = cv2.morphologyEx(red_mask, cv2.MORPH_OPEN, kernal)
    
    # Put mask over top of the original image.
    red_result = cv2.bitwise_and(frame, frame, mask = red_mask)
 
    # Show final output image
    cv2.imshow('green tracker', green_result)
    cv2.imshow('blue tracker', blue_result)
    cv2.imshow('yellow tracker', yellow_result)
    cv2.imshow('red tracker', red_result)

    cv2.imshow('trackers', frame)

    
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        print ("green_minH = %d\ngreen_maxH = %d\nyellow_minH = %d\nyellow_maxH = %d"%(lowHue_g,highHue_g,lowHue_y,highHue_y))
        print ("blue_minH = %d\nblue_maxH = %d\nred1_minH = %d\nred1_maxH = %d"%(lowHue_b,highHue_b,lowHue_r1,highHue_r1))
        print ("red2_minH = %d\nred2_maxH = %d"%(lowHue_r2,highHue_r2))
        print ("minS = %d\nmaxS = %d\nminV = %d\nmaxV = %d"%(lowSat, highSat, lowVal, highVal))   
        break


cv2.destroyAllWindows()
#lights.off()
