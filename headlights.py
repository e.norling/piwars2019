import time
import threading
import colorsys
from rgbmatrix5x5 import RGBMatrix5x5

arrows = [[4,9,14,19,24,8,13,18,12],
            [14,3,8,13,18,23,7,12,17,11],
            [14,13,2,7,12,17,22,6,11,16,10],
            [14,13,12,1,6,11,16,21,5,10,15],
            [14,13,12,11,0,5,10,15,20],
            [14],
            [9,14,19,13]
            ]
# constants for rainbow
spacing = 360.0 / 5.0
hue = 0

class Headlights(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.left_light = RGBMatrix5x5()
        self.right_light = RGBMatrix5x5(address=0x77)
        self.update_interval = 0.5
        self.updating = threading.Event()
        self.update_function = None
        self.side = None
        self.off()

    def run(self):
        while True:
            if not self.updating.is_set():
                self.updating.wait()
            if not self.update_function is None:
                self.update_function(side = self.side)
            time.sleep(self.update_interval)

    def off(self):
        self.updating.clear()
        self.side = None
        self.update_function = None
        self.left_light.clear()
        self.right_light.clear()
        self.left_light.show()
        self.right_light.show()

    def small_lights(self):
        self.left_light.clear()
        self.right_light.clear()
        pixels = [7,11,12,13,17]
        self.left_light.set_multiple_pixels(pixels, (0xff, 0xff, 0xff))
        self.right_light.set_multiple_pixels(pixels, (0xff, 0xff, 0xff))
        self.left_light.show()
        self.right_light.show()

    def solid(self, color=(0xff,0xff,0xff), side=None):
        (r,g,b) = color
        
        if side is None:
            self.left_light.set_all(r,g,b)
            self.right_light.set_all(r,g,b)
        else:
            side.set_all(r,g,b)
        self.left_light.show()
        self.right_light.show()
        
    def indicator(self, side):
        self.off()
        self.update_interval = 0.1
        self.updating.set()
        self.side = side
        self.update_function = self.indicator_update
        
    def indicator_update(self, side):
        colour = (0xff, 0xbf, 0x00)
        pattern = arrows.pop(0)
        arrows.append(pattern)
        side.clear()
        side.set_multiple_pixels(pattern, colour)
        side.show()

    def rainbow(self, side=None):
        if side is None:
            self.off()
        else:
            side.clear()
        self.update_interval = 0.0001
        self.updating.set()
        self.side = side
        self.update_function = self.rainbow_update

    def rainbow_update(self, side):
        width = self.left_light.width
        height = self.left_light.height
        for x in range(width):
            for y in range(height):
                hue = int(time.time() * 100) % 360
                offset = (x * y) / 25.0 * spacing
                h = ((hue + offset) % 360) / 360.0
                r, g, b = [int(c * 255) for c in colorsys.hsv_to_rgb(h, 1.0, 1.0)]
                if side is None:
                    self.left_light.set_pixel(x, y, r, g, b)
                    self.right_light.set_pixel(x, y, r, g, b)
                else:
                    side.set_pixel(x, y, r, g, b)

        if side is None:
            self.left_light.show()
            self.right_light.show()
        else:
            side.show()
