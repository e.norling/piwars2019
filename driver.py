# Code for Pi Zero Motor Shim driven robot
#
# By Emma Norling, based on code by Mike Horne for CamJam Edutkit 3.

# Need floating point division of integers
from __future__ import division
from time import sleep
from threading import Event, Thread
from bluetooth import *
from robotexception import RobotStopException

MIN = 30
MAX = 100
top_speed = MAX

running_gun = False

try:
    # Attempt to import the GPIO Zero library. If this fails, because we're running somewhere
    # that doesn't have the library, we create dummy functions for set_speeds and stop_motors which
    # just print out what they'd have done. This is a fairly common way to deal with hardware that
    # may or may not exist!

    # Use GPIO Zero generic robot implementation
    from gpiozero import Robot
    # And generic motors for the ones on the gun shim
    from gpiozero import Motor


    print('GPIO Zero found')

    # Get the robot instance and the independent motor controllers
    robot = Robot(left=(16,19),right=(26,20))
    motor_left = robot.left_motor
    motor_right = robot.right_motor

    height_motor = Motor(5,6)
    fire_motor = Motor(13,12)

    # Motors are reversed. If you find your robot going backwards, set this to 1
    motor_multiplier = -1


    def set_speeds(power_left, power_right):
        """
        As we have an motor hat, we can use the motors

        :param power_left: 
            Power to send to left motor
        :param power_right: 
            Power to send to right motor, will be inverted to reflect chassis layout
        """

        # If one wants to see the 'raw' 0-100 values coming in
        # print("source left: {}".format(power_left))
        # print("source right: {}".format(power_right))

        # Take the 0-100 inputs down to 0-1 and reverse them if necessary
        power_left = (motor_multiplier * power_left) / 100
        power_right = (motor_multiplier * power_right) / 100

        # Print the converted values out for debug
        # print("left: {}".format(power_left))
        # print("right: {}".format(power_right))

        # If power is less than 0, we want to turn the motor backwards, otherwise turn it forwards
        if power_left < 0:
            motor_left.backward(-power_left)
        else:
            motor_left.forward(power_left)

        if power_right < 0:
            motor_right.backward(-power_right)
        else:
            motor_right.forward(power_right)

    def adjust_height(power):
        # Take the 0-100 inputs down to 0-1 and reverse them if necessary
        power = (motor_multiplier * power) / 100
        #print("power: {}".format(power))

        # If power is less than 0, we want to turn the motor backwards, otherwise turn it forwards
        if power < 0:
            height_motor.backward(-power)
        else:
            height_motor.forward(power)

    def fire():
        print("fire!")
        fire_motor.forward(1)

    def stop_fire():
        print("stop")
        fire_motor.stop()

    def stop_motors():
        """
        As we have an motor hat, stop the motors using their motors call
        """
        # Turn both motors off
        motor_left.stop()
        motor_right.stop()

except ImportError:

    print('GPIO Zero not found, using dummy functions.')


    def set_speeds(power_left, power_right):
        """
        No motor hat - print what we would have sent to it if we'd had one.
        """
        print('DEBUG Left: {}, Right: {}'.format(power_left, power_right))
        sleep(0.1)


    def stop_motors():
        """
        No motor hat, so just print a message.
        """
        print('DEBUG Motors stopping')

def mixer(yaw, throttle, max_power=100):
    """
    Mix a pair of joystick axes, returning a pair of wheel speeds. This is where the mapping from
    joystick positions to wheel powers is defined, so any changes to how the robot drives should
    be made here, everything else is really just plumbing.
    
    :param yaw: 
        Yaw axis value, ranges from -1.0 to 1.0
    :param throttle: 
        Throttle axis value, ranges from -1.0 to 1.0
    :param max_power: 
        Maximum speed that should be returned from the mixer, defaults to 100
    :return: 
        A pair of power_left, power_right integer values to send to the motor driver
    """
    left = throttle + yaw
    right = throttle - yaw
    scale = float(max_power) / max(1, abs(left), abs(right))
    return int(left * scale), int(right * scale)

class Comms():

    def __init__(self, ready=None):
        self.running = False
        self.ready = ready

    def connect(self):
        if not self.ready.is_set():
            socket=BluetoothSocket( RFCOMM )
            socket.bind(("",PORT_ANY))
            socket.listen(1)

            port = socket.getsockname()[1]

            uuid = "5d122e1e-00fc-40af-82cc-bc71c6844a0f"

            advertise_service( socket, "DriverServer",
                               service_id = uuid,
                               service_classes = [ uuid, SERIAL_PORT_CLASS ],
                               profiles = [ SERIAL_PORT_PROFILE ], 
                            )
                       
            print ("Waiting for connection on RFCOMM channel %d" % port)

            self.client_socket, client_info = socket.accept()
            print ("Accepted connection from ", client_info)

            self.ready.set()

    def _is_connected(self):
        try:
            self.client_sock.getpeername()
            return True
        except (BluetoothError, AttributeError):
            return False

    def readline(self):
        try:
            return self.client_socket.recv(1024).decode("utf-8")
        except BluetoothError:
            self.ready.clear()
            raise RobotStopException

# Set up the communication with the "brains"
ready = Event()
comms = Comms(ready)

# Wait for the communication link to be properly established
thread = Thread(target=comms.connect)
thread.start()

while True:
    # block until ready
    ready.wait()
    # now we can safely use comms

    print("Established comms")
    
    counter = 0
    set_speeds(0, 0)
    import re

    while ready.is_set():
        try:
            counter = counter + 1
            line = comms.readline()
            #print("received ",line)
            left, right = [float(s) for s in re.findall(r'-?\d+\.?\d*', line)][:2]
            print('%d: %d %d' %(counter,left,right))
            set_speeds(left, right)
        except RobotStopException:
            print ("Must be a comms error; stop!!!")
            stop_motors()
            comms.connect()
            ready.wait()
        sleep(0.01)
                

