# Code for Pi Zero Motor Shim driven robot
#
# By Emma Norling, based on code by Mike Horne for CamJam Edutkit 3.

# Need floating point division of integers
from __future__ import division

from time import sleep

MIN = 30
MAX = 100
top_speed = MAX

running_gun = False

try:
    # Attempt to import the GPIO Zero library. If this fails, because we're running somewhere
    # that doesn't have the library, we create dummy functions for set_speeds and stop_motors which
    # just print out what they'd have done. This is a fairly common way to deal with hardware that
    # may or may not exist!

    # Use GPIO Zero generic robot implementation
    from gpiozero import Robot
    # And generic motors for the ones on the gun shim
    from gpiozero import Motor


    print('GPIO Zero found')

    # Get the robot instance and the independent motor controllers
    robot = Robot(left=(16,19),right=(26,20))
    motor_left = robot.left_motor
    motor_right = robot.right_motor

    height_motor = Motor(5,6)
    fire_motor = Motor(13,12)

    # Motors are reversed. If you find your robot going backwards, set this to 1
    motor_multiplier = -1


    def set_speeds(power_left, power_right):
        """
        As we have an motor hat, we can use the motors

        :param power_left: 
            Power to send to left motor
        :param power_right: 
            Power to send to right motor, will be inverted to reflect chassis layout
        """

        # If one wants to see the 'raw' 0-100 values coming in
        # print("source left: {}".format(power_left))
        # print("source right: {}".format(power_right))

        # Take the 0-100 inputs down to 0-1 and reverse them if necessary
        power_left = (motor_multiplier * power_left) / 100
        power_right = (motor_multiplier * power_right) / 100

        # Print the converted values out for debug
        # print("left: {}".format(power_left))
        # print("right: {}".format(power_right))

        # If power is less than 0, we want to turn the motor backwards, otherwise turn it forwards
        if power_left < 0:
            motor_left.backward(-power_left)
        else:
            motor_left.forward(power_left)

        if power_right < 0:
            motor_right.backward(-power_right)
        else:
            motor_right.forward(power_right)

    def adjust_height(power):
        # Take the 0-100 inputs down to 0-1 and reverse them if necessary
        power = (motor_multiplier * power) / 100
        #print("power: {}".format(power))

        # If power is less than 0, we want to turn the motor backwards, otherwise turn it forwards
        if power < 0:
            height_motor.backward(-power)
        else:
            height_motor.forward(power)

    def fire():
        print("fire!")
        fire_motor.forward(1)

    def stop_fire():
        print("stop")
        fire_motor.stop()

    def stop_motors():
        """
        As we have an motor hat, stop the motors using their motors call
        """
        # Turn both motors off
        motor_left.stop()
        motor_right.stop()

except ImportError:

    print('GPIO Zero not found, using dummy functions.')


    def set_speeds(power_left, power_right):
        """
        No motor hat - print what we would have sent to it if we'd had one.
        """
        print('DEBUG Left: {}, Right: {}'.format(power_left, power_right))
        sleep(0.1)


    def stop_motors():
        """
        No motor hat, so just print a message.
        """
        print('DEBUG Motors stopping')

# All we need, as we don't care which controller we bind to, is the ControllerResource
from approxeng.input.selectbinder import ControllerResource


class RobotStopException(Exception):
    """
    The simplest possible subclass of Exception, we'll raise this if we want to stop the robot
    for any reason. Creating a custom exception like this makes the code more readable later.
    """
    pass


def mixer(yaw, throttle, max_power=100):
    """
    Mix a pair of joystick axes, returning a pair of wheel speeds. This is where the mapping from
    joystick positions to wheel powers is defined, so any changes to how the robot drives should
    be made here, everything else is really just plumbing.
    
    :param yaw: 
        Yaw axis value, ranges from -1.0 to 1.0
    :param throttle: 
        Throttle axis value, ranges from -1.0 to 1.0
    :param max_power: 
        Maximum speed that should be returned from the mixer, defaults to 100
    :return: 
        A pair of power_left, power_right integer values to send to the motor driver
    """
    left = throttle + yaw
    right = throttle - yaw
    scale = float(max_power) / max(1, abs(left), abs(right))
    return int(left * scale), int(right * scale)


# Outer try / except catches the RobotStopException, which we'll raise when we want to
# bail out of the loop cleanly, shutting the motors down. We can raise this in response to a button press
try:
    while True:
        # Inner try / except is used to wait for a controller to become available, at which point we
        # bind to it and enter a loop where we read axis values and send commands to the motors.
        try:
            # Bind to any available joystick, this will use whatever's connected as long as the library
            # supports it.
            with ControllerResource(dead_zone=0.1, hot_zone=0.2) as joystick:
                print('Controller found, use left stick to drive.')
                print(joystick.controls)
                # Loop until the joystick disconnects, or we deliberately stop by raising a
                # RobotStopException
                while joystick.connected:
                    joystick.check_presses()
                    # Print out any buttons that were pressed, if we had any
                    if joystick.has_presses:
                        print(joystick.presses)

                    # Get joystick values from the left analogue stick
                    # and the y axis of the right analogue stick
                    x_axis, y_axis, gun_axis = joystick['lx', 'ly', 'ry']
                    # Get power from mixer function
                    power_left, power_right = mixer(yaw=x_axis, throttle=y_axis, max_power=top_speed)
                    # Set motor speeds
                    set_speeds(power_left, power_right)
                    # Adjust gun height
                    adjust_height(gun_axis*100.0)
                    # Get a ButtonPresses object containing everything that was pressed since the last
                    # time around this loop.
                    if 'dup' in joystick.presses:
                        top_speed = min(top_speed +10,MAX)
                    if 'ddown' in joystick.presses:
                        top_speed = max(top_speed-10,MIN)
                    #print('top speed is now ', top_speed)
                    if 'r2' in joystick.presses:
                        if not running_gun:
                            fire()
                        else:
                            stop_fire()
                        running_gun = not running_gun
                    
                    sleep(0.01)
                        
        except IOError:
            # We get an IOError when using the ControllerResource if we don't have a controller yet,
            # so in this case we just wait a second and try again after printing a message.
            print('No controller found yet')
            sleep(1)
            
except (RobotStopException, KeyboardInterrupt):
    stop_motors()

